#list basics

empty=[]
empty=list()

print(empty)

name=["rudra","abc","pqr"]
print(name)

print("pqr" in name)
print("xyz" in name)

print()
print("rudra" not in name)

test_scores=[99.0,35.0,22.5]

print()
print(33 in test_scores)
print(35 in test_scores)
print(35.0 in test_scores)

